Template.auctionBuilder.helpers({

	serverTime: function() {
		var date = new Date(TimeSync.serverTime(null, 1000));
		return date;
	},

	privateAuction: function() {
		var temp = Session.get("privateAuction");
		console.log("temp: " + temp);
		return temp == "true";
	},

	friend: function() {
		// Due to formatting this helper must return a list of rows
		var user = Meteor.users.findOne({ _id : Meteor.userId()});
		var formattedArray = []; // The formatted array that will be returned

		if (user && user.friends) {
			var friends = user.friends;
			size = 4; // How many items in the row
			while (friends.length > size) {
				formattedArray.push({ row: friends.slice(0, size)});
				friends = friends.slice(size);
			}
	        formattedArray.push({row: friends});
	    }
	    return formattedArray;
	},

	friendDocument: function() {
		return Meteor.users.findOne({_id:this._id});
	}
})

Template.auctionBuilder.events({

	'submit #new-auction': function(evt) {

		evt.preventDefault();
		console.log('form submitted.');

		var auctionName = $('#auction-name').val();
		var auctionSeller = Meteor.userId();
		var startPrice = $('#start-price').val();
		var reservePrice = $('#reserve-price').val();
		var auctionDirection = $('#auction-direction').val();
		var auctionType = $('#auctionType').val();
		var endDate = $('#end-date').val();
		var endTime = $('#end-time').val();
		var public = null;
		var invitedBidders = []
		var endDateTime = endDate + ':' + endTime;

		if ($('#btn-private').is(':checked')) {
			console.log('private');
			public = false;
		} else {
			public = true;
		}

		$( ".select-bidders-checkbox" ).each(function() {
			if ($(this).prop('checked')) {
				console.log('checked');
				invitedBidders.push($(this).val());
			}
		});
		
		if (auctionType == "blind") {
			Meteor.call('insertNewBlindAuction', auctionName, auctionSeller, auctionDirection, public, startPrice, reservePrice,  endDateTime, function(error, result) {
				if (error) {
					alert('Error inserting new Auction: ' + error);
				} else {
					$('#success-fail').html('Success!');
					console.log('result._id: ' + result);
					Meteor.call('addUsersToAuction', result.valueOf(), invitedBidders);
				}
			});
		}
		
	},
	'change #btn-public' : function (){
		console.log('1');
		Session.set("privateAuction", false);
	},

	'change #btn-private' : function (){
		console.log('2');
		Session.set("privateAuction", false);
	}

})