Template.auctionPage.helpers({

	serverTime: function() {
		var date = new Date(TimeSync.serverTime(null, 1000));
		return date;
	},

	timeRemaining: function() {
		var end = this.endDateTime;
		var serverTime = new Date(TimeSync.serverTime(null, 1000));

		var duration = moment.duration(end - serverTime, 'milliseconds');

		return duration.days() + ":" + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
	}

});

Template.auctionPage.events({

	'submit #new-bid': function(evt) {

		evt.preventDefault();
		// Check if bid is valid before calling the server 
		// NOTE: Checks also performed on the server
		var auctionId = Router.current().params.auctionIdentifier;
		auctionId = $.trim(auctionId);
		console.log('auctionId: ' + auctionId);
		var bidAmount = $('#bid-amount').val();
		var winningBid = $('#winning-bid').html();
		var auctionType = $('#auction-type').html();
		var auctionType = $.trim(auctionType);
		var bidder = "james";

		console.log('auctionType: ' + auctionType);
		if (auctionType == "ascending") {
			if (bidAmount <= winningBid) {
				alert('Your bid is not greater than the current winning bid (' + winningBid + '). Please enter a higher bid.');
			} else {
				// submit bid.
				Meteor.call('insertNewBid', auctionId, bidAmount, bidder);
			}
		} else if (auctionType == "descending") {
			if (bidAmount >= winningBid) {
				alert('Your bid is not lower than the current winning bid (' + winningBid + '). Please enter a lower bid.');
			} else {
				// submit bid.
				Meteor.call('insertNewBid', auctionId, bidAmount, bidder);
			}
		}
	}

});