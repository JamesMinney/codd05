Meteor.methods({
	/**
	*	@summary Adds a new entry to the 'Users' collection representing a new user of the system.
    *   User is added without full access until verification if achieved.
    *	@method createNewUser
    *	@param {String} aFirstname The new Users's firstname.
    *	@param {String} aSurname The new Users's surname.
    *	@param {String} aEmail The new User's email.
    *	@param {String} aCompany The comapny the new User belongs to.
    *	@param aPassword An encrypted password the user has registered with.
    *   
    */
    'createNewUser': function(aFirstname, aSurname, aEmail, aSector, aCompany, aPassword) {
        var email = aEmail;
        var password = aPassword;
        var company = aCompany;
        var profile = {
            firstname: aFirstname,
            surname: aSurname,
            sector: aSector
        };
        var notifications = [];
        var friends = [];
        var friendRequests = [];
        var watchedAuctions = [];
        var myAuctions = [];
        var auctionInvites = [];

        return Accounts.createUser({email: email, password: password, profile: profile, company: company, friends: friends, friendRequests: friendRequests, auctionInvites: auctionInvites, watchedAuctions: watchedAuctions, myAuctions: myAuctions})
    },

    /**
    *   @summary Sends a Friend Request to 'Friend' by adding an entry to friendRequest subdocument
    *   @method addFriend
    *   @param {String} friend The Friend being added _id
    *   
    */
    'addFriend': function(friend) {
        var currentDate = new Date();
        var currentUser = Meteor.users.findOne({_id: Meteor.userId()});
        
        if (Meteor.users.update({ _id : Meteor.userId() }, {$addToSet: {friendRequests: {friendId: friend, date: currentDate, sender: true, accepted: null}}})) {
            return Meteor.users.update({_id: friend}, {$addToSet: {friendRequests: {friendId: Meteor.userId(), friendName: currentUser.profile.firstname + " " + currentUser.profile.surname, company: currentUser.company, date: currentDate, sender: false, accepted: null, unread: true}}})
        }
    },

     /**
    *   @summary Removes the User calling the method from friendId friendlist and vice-versa.
    *   @method removeFriend
    *   @param {String} friendId The ID of the friend being removed.
    *   
    */   
    'removeFriend': function(friendId) {
        if (Meteor.users.update({ _id: Meteor.userId() }, {$pull: { friends: { _id : friendId } } })) {
            Meteor.users.update({ _id: friendId }, {$pull: { friends: { _id : Meteor.userId() } } })
        }
    },

    /**
    *   @summary Sets the friend friendId to the value of grade
    *   @method changeAssignedGrade
    *   @param {String} friend The Friend being added _id
    *   @param {Integer} grade The new grade being assigned
    *   
    */
    'changeAssignedGrade': function(friendId, grade) {
        if (Meteor.userId()) {
            var user = Meteor.users.findOne({_id: Meteor.userId()});
            var isFriend = false;
            for (var i = 0; i < user.friends.length; i++) {
                if (user.friends[i]._id == friendId) {
                    isFriend = true;
                    break;
                }
            }

            if (isFriend) {
                return Meteor.users.update({_id:Meteor.userId(), "friends._id": friendId}, {$set: {"friends.$.assignedGrade": grade}})
            } else {
                throw new Meteor.Error("User is not a friend!");
            }
        }
    },

    /**
    *   @summary Change the User's bio to newBio
    *   @method updateUserBio
    *   @param {String} newBio The new entry to the profile bio
    *   
    */
    'updateUserBio': function(newBio) {
        if (Meteor.userId()) {
            return Meteor.users.update({_id: Meteor.userId()}, {$set: {"profile.description": newBio}})
        }
    },

    /**
    *   @summary Accept or Reject a friend request
    *   @method handleFriendRequest
    *   @param {String} friend The friend who's request is being confirmed
    *   @param {String} response Either 'Accept' or 'Reject'
    *   
    */
    'handleFriendRequest': function(friend, response) {

        if (Meteor.userId()) {
            // Get the documents from the Users collection
            userDocument = Meteor.users.findOne({_id : Meteor.userId()});
            var friendRequestExists = false;
            for (var i = 0; i < userDocument.friendRequests.length; i++) {
                if (userDocument.friendRequests[i].friendId == friend.friendId) {
                    friendRequestExists = true;
                }
            }
            if (!friendRequestExists){
                throw new Meteor.Error("Cannot accept non-existent friend request.");
            }
            if (response == "accept" || friendRequestExists) {
                var currentDate = new Date();
                friendDocument = Meteor.users.findOne({_id : friend.friendId});

                userId = userDocument._id;
                friendDocId = friendDocument._id;

                // Remove Friend Requests.
                Meteor.users.update({_id : Meteor.userId()}, {$pull : { friendRequests :  { friendId : friend.friendId }}});
                Meteor.users.update({_id : friendDocId}, {$pull : { friendRequests : { friendId : Meteor.userId() }}});

                // Add Users as Friends.
                Meteor.users.update({_id : Meteor.userId()}, { $addToSet: { friends : { _id : friend.friendId, date: currentDate, assignedGrade: 0 }}});
                Meteor.users.update({_id : friend.friendId}, { $addToSet: { friends : { _id : Meteor.userId(), date: currentDate, assignedGrade: 0 }}});

            } else if (response == "reject" || friendRequestExists) {
                Meteor.users.update({_id : Meteor.userId()}, {$pull : { friendRequests :  { friendId : friend.friendId }}});
                Meteor.users.update({_id : friendDocId}, {$pull : { friendRequests : { friendId : Meteor.userId() }}});
            }
        }
    },

    /**
    *   @summary Change the Profile pic of User by changing the id of the profile.picture in this users document
    *   @method addProfilePic
    *   @param {String} profilePicId The id of the image in the ProfilePics collection
    *   
    */
    'addProfilePic': function(profilePicId) {
        if (Meteor.userId()) {
            Meteor.users.update({ _id:Meteor.userId() }, { $set: { "profile.picture": profilePicId }})
        }
    }
})