// server/smtp.js
Meteor.startup(function () {
  smtp = {
    username: 'james@klik2learn.com',   // eg: server@gentlenode.com
    password: 'yose9MightY',   // eg: 3eeP1gtizk5eziohfervU
    server:   'smtp.gmail.com',  // eg: mail.gandi.net
    port: 587
    // Changed from '25' to remove ECONNREFUSED error (9th Feb 2016)
  }

  process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;
	
	// By default, the email is sent from no-reply@meteor.com. If you wish to receive email from users asking for help with their account, be sure to set this to an email address that you can receive email at.
  Accounts.emailTemplates.from = 'James <james@klik2learn.com>';

  // The public name of your application. Defaults to the DNS name of the application (eg: awesome.meteor.com).
  //Accounts.emailTemplates.siteName = 'K2L Demo Test';

  // A Function that takes a user object and returns a String for the subject line of the email.
  Accounts.emailTemplates.verifyEmail.subject = function(user) {
    return 'Confirm Your Email Address';
  };

  // A Function that takes a user object and a url, and returns the body text for the email.
  // Note: if you need to return HTML instead, use Accounts.emailTemplates.verifyEmail.html
  Accounts.emailTemplates.verifyEmail.text = function(user, url) {
    return 'Click on the following link to verify your email address: ' + url;
  };
});

Accounts.onCreateUser(function(options, user) {

  user.profile = options.profile;
  user.company = options.company;
  user.friends = options.friends;
  user.friendRequests = options.friendRequests;
  user.auctionInvites = options.auctionInvites;
  user.watchedAuctions = options.watchedAuctions;
  user.myAuctions = options.myAuctions;

  // we wait for Meteor to create the user before sending an email
  Meteor.setTimeout(function() {
    Accounts.sendVerificationEmail(user._id);
  }, 2 * 1000);

  return user;
});

// (server-side) called whenever a login is attempted
Accounts.validateLoginAttempt(function(attempt){
  if (attempt.user && attempt.user.emails && !attempt.user.emails[0].verified ) {
    return false; // the login is aborted
  }
  return true;
}); 