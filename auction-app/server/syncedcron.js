SyncedCron.config({
	collectionName: 'syncedcron'
});


/*
*	This job runs every minute. It is responsible for finding any auctions which have finished in the 
*	last minute and sending out emails to the users involved.
*
*/
SyncedCron.add({

	name: 'Check auctions end time against server clock and finish them if their time has elapsed',
	schedule: function(parser){
		return parser.recur().every(60).second().startingOn(1);
	},

	job: function() {
		// Return a list of auctions which have finished
		var recentlyFinishedAuctions = checkAuctions();
		updateAuctions(recentlyFinishedAuctions);
		// Send notification Emails that auctions have ended
		sendAuctionEndEmails(recentlyFinishedAuctions);
	}

})

/*
*	This job runs every minute. It is responsible for reducing the price of all
*	Dutch Auctions whos interval has elapsed in the last minute.
*
*/
SyncedCron.add({

	name: 'Check Dutch auctions to reduce the price',
	schedule: function(parser){
		return parser.text('every 1 mins starting on the 0th min');
	},

	job: function() {
		var reducedAuctions = [];
		reducedAuctions = checkDutchAuctions();
		sendAuctionEndEmails(reducedAuctions);
	}

})


/**
*   @summary This function checks each dutch auction and reduces the price if the interval has passed. It then sets the auction documents next reduce time based on the interval.
*	@method checkDutchAuctions
*   
*/
function checkDutchAuctions() {

	var recentlyFinished = [];
	var dutchAuctions = auctions.find({finished:false, auction_type: "dutch"}).fetch() // 
	var currentTime = new Date();

	for (var i = 0; i < dutchAuctions.length; i++) {
		if (dutchAuctions[i].nextReduceTime <= currentTime) { // The price is due to be reduced.
			if (dutchAuctions[i].auction_direction == "ascending") {
				var newPrice = dutchAuctions[i].currentPrice - dutchAuctions[i].reductionAmount;
				var reductionInterval = dutchAuctions[i].reductionInterval;
				var nextReduceTime = {};
		        nextReduceTime = addToDate(dutchAuctions[i].nextReduceTime, "day", reductionInterval[0]);
		        nextReduceTime = addToDate(nextReduceTime, "hour", reductionInterval[1]);
		        nextReduceTime = addToDate(nextReduceTime, "minute", reductionInterval[2]);
				if (newPrice <= dutchAuctions[i].reservePrice) {
					auctions.update({ _id : dutchAuctions[i]._id }, { $set: { finished : true }});
					recentlyFinished.push(dutchAuctions[i]);
				} else {
					auctions.update({ _id : dutchAuctions[i]._id }, { $set: { currentPrice : newPrice, nextReduceTime : nextReduceTime }});
				}
			} else if (dutchAuctions[i].auction_direction == "descending") {
				var newPrice = parseInt(dutchAuctions[i].currentPrice) + parseInt(dutchAuctions[i].reductionAmount);
				var reductionInterval = dutchAuctions[i].reductionInterval;
				var nextReduceTime = {};
		        nextReduceTime = addToDate(dutchAuctions[i].nextReduceTime, "day", reductionInterval[0]);
		        nextReduceTime = addToDate(nextReduceTime, "hour", reductionInterval[1]);
		        nextReduceTime = addToDate(nextReduceTime, "minute", reductionInterval[2]);
				if (newPrice >= dutchAuctions[i].reservePrice) {
					auctions.update({ _id : dutchAuctions[i]._id }, { $set: { finished : true }});
					recentlyFinished.push(dutchAuctions[i]);
				} else {
					auctions.update({ _id : dutchAuctions[i]._id }, { $set: { currentPrice : newPrice, nextReduceTime : nextReduceTime }});
				}
			}
		}
	}
	return recentlyFinished;
}


/**
*   @summary This function checks each unfinished auction and checks if the endDateTime has passed. If it has, it sets the auction to finished. The function returns a list of recently finished auctions.
*	@method checkAuctions
*	@returns {Array} Auctions which have recently finished
*   
*/
function checkAuctions() {

	var recentlyFinished = [];
	var unfinishedAuctions = auctions.find({finished: false}).fetch()
	var currentTime = new Date();

	for (var i = 0; i < unfinishedAuctions.length; i++) {
		if (unfinishedAuctions[i].endDateTime <= currentTime) {
			recentlyFinished.push(unfinishedAuctions[i]);
			auctions.update({_id: unfinishedAuctions[i]._id}, {$set: {finished: true}});
		}
	}
	return recentlyFinished;
}

/**
*   @summary This is an important funciton in the system. This function calculates the winner of each auction and writes the result to the auctions document in the database.
*	@method updateAuctions
*	@params	{Array} finishedAuctions An array of auctions which have recently finished and need to be processed.
*   
*/
function updateAuctions(finishedAuctions) {

	for (var i = 0; i < finishedAuctions.length; i++) {
		if (finishedAuctions[i].auction_type == "blind" || finishedAuctions[i].auction_type == "english") {

			// Check if the auction has received any bids..
			if (finishedAuctions[i].bids.length == 0) {
				return;
			}

			if (finishedAuctions[i].auction_direction == "ascending") {
				var bids = finishedAuctions[i].bids;
				var highestBidAmount = bids[0].amount;
				var highestBid = [];
				highestBid.push(bids[0]);
				for (var j = 0; j < bids.length; j++) {
					if (parseInt(bids[j].amount) > parseInt(highestBidAmount)) {
						highestBid = [];
						highestBid.push(bids[j]);
						highestBidAmount = bids[j].amount;
					} else if (parseInt(bids[j].amount) == parseInt(highestBidAmount)) {
						highestBid.push(bids[j]);
					}
				}
				for (var k = 0; k < highestBid.length; k++) {
					auctions.update({_id: finishedAuctions[i]._id}, { $addToSet: {winner: {_id: highestBid[k].bidder, selectedOptions: highestBid[k].selectedOptions, amount: highestBid[k].amount}}});
				}
			} else if (finishedAuctions[i].auction_direction == "descending") {
				var bids = finishedAuctions[i].bids;
				var lowestBidAmount = bids[0].amount;
				var lowestBid = [];
				lowestBid.push(bids[0]);
				for (var j = 0; j < bids.length; j++) {
					if (parseInt(bids[j].amount) < parseInt(lowestBidAmount)) {
						lowestBid = [];
						lowestBid.push(bids[j]);
						lowestBidAmount = bids[j].amount;
					} else if (parseInt(bids[j].amount) == parseInt(lowestBidAmount)) {
						lowestBid.push(bids[j]);
					}
				}
				for (var k = 0; k < lowestBid.length; k++) {
					auctions.update({_id: finishedAuctions[i]._id}, { $addToSet: {winner: {_id: lowestBid[k].bidder, selectedOptions: lowestBid[k].selectedOptions, amount: lowestBid[k].amount}}});
				}
			}
		} else if (finishedAuctions[i].auction_type == "secondprice") {
			if (finishedAuctions[i].auction_direction == "ascending") {
				var bids = finishedAuctions[i].bids;
				var highestBidAmount = bids[0].amount;
				var highestBid = [];
				var secondBidAmount = undefined;
				var secondBid = [];
				//highestBid.push(bids[0]);
				for (var j = 0; j < bids.length; j++) {
					if (parseInt(bids[j].amount) > parseInt(highestBidAmount)) {
						secondBid = highestBid;
						highestBid = [];
						highestBid.push(bids[j]);
						secondBidAmount = highestBidAmount;
						highestBidAmount = bids[j].amount;
					} else if (parseInt(bids[j].amount) == parseInt(highestBidAmount)) {
						highestBid.push(bids[j]);
					} else if (parseInt(bids[j].amount) < parseInt(highestBidAmount)) {
						if (parseInt(bids[j].amount) > parseInt(secondBidAmount) || secondBidAmount == undefined) {
							secondBidAmount = bids[j].amount;
							secondBid = [];
							secondBid.push(bids[j]);
						}
					}
				}
				for (var k = 0; k < highestBid.length; k++) {
					auctions.update({_id: finishedAuctions[i]._id}, { $addToSet: {winner: {_id: highestBid[k].bidder, amount: highestBid[k].amount, selectedOptions: highestBid[k].selectedOptions, secondAmount: secondBid[0].amount }}});
				}
			} else if (finishedAuctions[i].auction_direction == "descending") {
				var bids = finishedAuctions[i].bids;
				var lowestBidAmount = bids[0].amount;
				var lowestBid = [];
				var secondBidAmount = undefined;
				var secondBid = [];
				lowestBid.push(bids[0]);
				for (var j = 0; j < bids.length; j++) {
					if (parseInt(bids[j].amount) < parseInt(lowestBidAmount)) {
						console.log('lowestBid[0]: ' + lowestBid[0]);
						secondBid = lowestBid;
						lowestBid = [];
						lowestBid.push(bids[j]);
						secondBidAmount = lowestBidAmount;
						lowestBidAmount = bids[j].amount;
					} else if (parseInt(bids[j].amount) == parseInt(lowestBidAmount)) {
						lowestBid.push(bids[j]);
					} else if (parseInt(bids[j].amount) > parseInt(lowestBidAmount) ) {
						if (parseInt(bids[j].amount) < parseInt(secondBidAmount)  || secondBidAmount == undefined) {
							secondBidAmount = bids[j].amount;
							secondBid = [];
							secondBid.push(bids[j]);
						}
					}
				}

				for (var k = 0; k < lowestBid.length; k++) {
					auctions.update({_id: finishedAuctions[i]._id}, { $addToSet: {winner: {_id: lowestBid[k].bidder, amount: lowestBid[k].amount, selectedOptions: lowestBid[k].selectedOptions, secondAmount: secondBid[0].amount }}});
				}
			}
		}
	}
}

/***
*	This function is used to send compose and send notification emails
* 	that an auction has ended.
*
*
*/
function sendAuctionEndEmails(auctions) {

	var to = []; // Array of recipients of the email
	var from = "james@klik2learn.com";
	var subject = "An Auction has ended";
	// Send a new Email for every auction that has finished.
	for (var i = 0; i < auctions.length; i++) {
		// Send an email to the users that listed the auction.
		var seller = auctions[i].seller;
		var user = Meteor.users.findOne({_id : seller});
		to.push(user.emails[0].address);

		// Also send the email to users that bidded on the auction.
		var sendListIds = [] // Temporary list to hold Ids of invited bidders and actual bidders
		
		// Ids for all users who were invited to the auction
		for (var j = 0; j < auctions[i].invitedBidders.length; j++) {
			var found = sendListIds.indexOf(auctions[i].invitedBidders[j]);
			if (found < 0) {
				sendListIds.push(auctions[i].invitedBidders[j])
			}
		}

		// Ids for all users who bidded on the auction (Important for public auctions but included
		//	as a defensive programming approach).
		for (var k = 0; k < auctions[i].bids.length; k++) {
			var found = sendListIds.indexOf(auctions[i].bids[k].bidder);
			if (found < 0) {
				sendListIds.push(auctions[i].bids[k].bidder); // Add the bidder to the list of Ids.
			}
		}

		// Iterate through the sendListIds and get email addresses
		for (var l = 0; l < sendListIds.length; l++) {
			var user = Meteor.users.findOne({_id : sendListIds[l]});
			to.push(user.emails[0].address); // Add the user's email address to the 'to' list
		}

		// Set the text for the email
		text = "An auction you were involved in has recently ended. Please visit http://theauctionwall.com/"+auctions[i].auction_type +"/" + auctions[i]._id + " to view more details."

		// Send the email
		Email.send({
			bcc: to,
			from: from,
			subject: subject,
			text: text
		});	

	}
}

function addToDate(date, unit, quantity) {
  var currentTime = new Date(date); //don't change original date
  switch(unit.toLowerCase()) {
    case 'minute':
        currentTime.setTime(currentTime.getTime() + quantity*60000);
        break;
    case 'hour':
        currentTime.setTime(currentTime.getTime() + quantity*3600000);
        break;
    case 'day':
        currentTime.setDate(currentTime.getDate() + quantity);
        break;
  }
  return currentTime;
}
