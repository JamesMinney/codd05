Meteor.methods({
    /**
    *   @summary DEPRECATED This was the original method for syncing the server time to the client. It can still be used to get the server time
    *   @method getServerTime
    *   
    */
    getServerTime: function () {
        var _time = (new Date).toTimeString();
        return _time;
    },

    /**
    *   @summary DEPRECATED This was the original method for syncing the server time to the client. It can still be used to get the server time
    *   
    *	@method markAsRead
	*   @param {String} notificationObject The notification to be removed
	*	@param {String} notificationType The type of notification, used to ensure the correct field is altered
    *   
    */
    markAsRead: function(notificationObject, notificationType) {
    	if (notificationType == "auctionInvite") {
    		Meteor.users.update({_id : Meteor.userId()}, {$pull : {auctionInvites : notificationObject } });
    	}
    }

});
