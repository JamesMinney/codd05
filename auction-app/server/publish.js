Meteor.publish("userData", function() {
    if (this.userId) {
        return Meteor.users.find({}, 
            {fields:
                {"profile": 1,
                "company": 1,
                "emails.address": 1 }
            });
    }
});

Meteor.publish("userList", function() {
	return Meteor.users.find({_id: this.userId},
        {fields: 
            {"emails.address": 1, 
            "profile": 1, "company": 1, 
            "friendRequests":1, 
            "friends":1, 
            "watchedAuctions":1, 
            "auctionInvites":1, 
            "myAuctions":1}
        });
});

Meteor.publish("auctionImageData", function() {
    return images.find({});
})

Meteor.publish("profileImages", function() {
    return profilePics.find({});
})

Meteor.publish("sectors", function() {
    return sectors.find({});
})

Meteor.publish("auctionDoc", function(auctionId) {
    var auction = auctions.findOne({_id: auctionId});
    var auctionWinner = false;
    if (auction.winner) {
        if (auction.winner.length > 0) {
           for (var i = 0; i < auction.winner.length; i++) {
                if (auction.winner[i]._id == this.userId) {
                    auctionWinner = true;
                    break;
                }
            } 
        } else if (auction.winner && auction.winner._id == this.userId) {
            auctionWinner = true;
        }
    } else {
        auctionWinner = false;
    }
    //
    if (auction.seller == this.userId) {
        return auctions.find({_id : auctionId},
            {   fields: {
                    "auction_name": 1,
                    "auction_description": 1,
                    "auction_direction":1,
                    "seller": 1, 
                    "auction_type": 1,
                    "auction_options": 1,
                    "public": 1,
                    "bids": 1,
                    "startPrice": 1,
                    "currentPrice":1,
                    "reservePrice":1,
                    "winner":1,
                    "reductionAmount":1,
                    "reductionInterval":1,
                    "nextReduceTime":1,
                    "startDateTime": 1,
                    "endDateTime": 1,
                    "finished": 1
                }
            })
    } else if (auction.finished && auctionWinner == true) {
        return auctions.find({_id: auctionId},
            {   fields: {
                    "auction_name": 1,
                    "auction_description": 1,
                    "auction_direction":1,
                    "seller": 1, 
                    "auction_type": 1,
                    "auction_options": 1,
                    "public": 1,
                    "bids.amount": 1,
                    "bids.date": 1,
                    "startPrice": 1,
                    "currentPrice": 1,
                    "reservePrice": 1,
                    "winner": 1,
                    "reductionAmount":1,
                    "reductionInterval":1,
                    "nextReduceTime":1,
                    "startDateTime": 1,
                    "endDateTime": 1,
                    "finished": 1
                }
            })
    } else if (!auction.public) {
        var invitedBidders = auction.invitedBidders;
        for (var i = 0; i < auction.invitedBidders.length; i++) {
            if (invitedBidders[i].bidder = this.userId) {
                return auctions.find({_id : auctionId},
                    {   fields: {
                            "auction_name": 1,
                            "auction_description": 1,
                            "auction_direction":1,
                            "seller": 1, 
                            "auction_type": 1,
                            "auction_options": 1,
                            "public": 1,
                            "bids.date":1,
                            "bids.amount":1,
                            "bidStep":1,
                            "startPrice": 1,
                            "currentPrice":1,
                            "reservePrice":1,
                            "winner.price":1,
                            "winner.amount":1, 
                            "winner.date": 1,
                            "reductionAmount":1,
                            "reductionInterval":1,
                            "nextReduceTime":1,
                            "startDateTime": 1,
                            "endDateTime": 1,
                            "finished": 1
                        }
                    })
            }
        }
    } else if (auction.public) {
        return auctions.find({_id: auctionId},
            {   fields: {
                    "auction_name": 1,
                    "auction_description": 1,
                    "auction_direction":1,
                    "seller": 1, 
                    "auction_type": 1,
                    "auction_options": 1,
                    "public": 1,
                    "bids.date":1,
                    "bids.amount":1,
                    "bidStep":1,
                    "startPrice": 1,
                    "currentPrice":1,
                    "reservePrice":1,
                    "winner.price":1,
                    "winner.date": 1,
                    "reductionAmount":1,
                    "reductionInterval":1,
                    "nextReduceTime":1,
                    "startDateTime": 1,
                    "endDateTime": 1,
                    "finished": 1
                }
            })
    }
})
// PUBLIC AUCTIONS
Meteor.publish("publicAuctions", function() {
    return auctions.find({"public": true, "seller" :{ $ne: this.userId }, "winner._id" :{ $ne: this.userId}},
        {   fields: {
                "auction_name": 1,
                "auction_description": 1,
                "auction_direction":1,
                "seller": 1, 
                "auction_type": 1,
                "auction_options": 1,
                "public": 1,
                "bids.date":1,
                "bids.amount":1,
                "bidStep":1,
                "startPrice": 1,
                "currentPrice":1,
                "reservePrice":1,
                "winner.amount":1,
                "winner.price":1,
                "winner.date": 1,
                "reductionAmount":1,
                "reductionInterval":1,
                "nextReduceTime":1,
                "startDateTime": 1,
                "endDateTime": 1,
                "finished": 1
            }
        })
});


// INVITED USER
Meteor.publish("invitedBidderAuctions", function() {
    return auctions.find({"invitedBidders": this.userId}, {$and: [ {"winner._id" : {$ne: this.userId}}, {"auction_type": {$ne: "blind"}}]},
        {   fields: {
                "auction_name": 1,
                "auction_description": 1,
                "auction_direction":1,
                "seller": 1, 
                "auction_type": 1,
                "auction_options": 1,
                "public": 1,
                "bids.date":1,
                "bids.amount":1,
                "bidStep":1,
                "startPrice": 1,
                "currentPrice":1,
                "reservePrice":1,
                "winner.price":1,
                "winner.date": 1,
                "winner.amount":1,
                "reductionAmount":1,
                "reductionInterval":1,
                "nextReduceTime":1,
                "startDateTime": 1,
                "endDateTime": 1,
                "finished": 1
            }
        })
});

Meteor.publish("sellerAuctions", function() {
    return auctions.find({"seller": this.userId},
        {   fields: {
                "auction_name": 1,
                "auction_description": 1,
                "auction_direction":1,
                "seller": 1, 
                "auction_type": 1,
                "auction_options": 1,
                "public": 1,
                "bids": 1,
                "startPrice": 1,
                "currentPrice":1,
                "reservePrice":1,
                "winner":1,
                "reductionAmount":1,
                "reductionInterval":1,
                "nextReduceTime":1,
                "startDateTime": 1,
                "endDateTime": 1,
                "finished": 1
            }
        })
});

Meteor.publish("auctionSellerDoc", function(auctionId) {
    return auctions.findOne({_id: auctionId, seller: this.userId})
})

Meteor.publish("bidData", function() {
    return bids.find({"type":"english"},
        {fields: {
            "auction": 1,
            "date": 1,
            "amount": 1
        }})
});

Meteor.publish("userBidData", function() {
    return bids.find({
        bidder: this.userId
    })
});