Meteor.methods({
	/**
    *   @summary Adds a new blind auction the auctions collection
    *   @method insertNewBlindAuction
    *   @param {Object} aAuction This object contains all the data needed to create the auction including; name, description etc.
    *   
    */
    insertNewBlindAuction: function(aAuction) {
    	// The parameters can be checked securely on the server in this method. (To be added..)
    	// e.g if (aReservePrice < 0)
        var auction = aAuction;
        checkAuction(auction);

        var startDateTime = {}; 
        startDateTime = checkStartDateTime(auction.auctionStartDate);

        var endDateTime = {};
        endDateTime = checkEndDateTime(auction.auctionEndDate);
        endDateTime.setSeconds(0,-1);
	    return auctions.insert({
    	   auction_name: auction.auctionName,
    	   seller: Meteor.userId(),
           auction_description: auction.auctionDescription,
           auction_direction: auction.auctionDirection,
           auction_type: "blind",
           auction_options: auction.checkedOptions,
           invitedBidders: [],
           bids: [],
           winner: [],
           images: [],
           public: auction.auctionPublic,
     	   reservePrice: auction.auctionReserve,
           startDateTime: startDateTime,
    	   endDateTime: endDateTime,
           finished: false
    	}, function(error, result) { 
            if (error) {
                console.log('error');
                throw new Meteor.Error("Insert failed.");
            } else {
                Meteor.users.update({ _id: Meteor.userId() }, { $addToSet: { myAuctions: result.valueOf() }});
                Meteor.call('addUsersToAuction', result.valueOf(), auction.invitedBidders);
            }
        });
    },
	/**
	*	@summary Insert a bid into a blind auction. A user is only allowed one bid in a blind auction.
    *	@method insertBlindBid
    *	@param {Integer} aBidAmount The value the user is bidding.
    *   @param {Array}  aSelectedOptions The options the user has selected to accompany their bid.
    *	@param {String} aAuctionId The id of the auction the user is bidding on. This must be a blind auction. 
    *   
    */
    insertBlindBid: function(aBidAmount, aSelectedOptions, aAuctionId) {
    	// get the auction associated with aAuctionId
    	var auction = auctions.findOne({ _id : aAuctionId });
    	// Check if the user has authorisation to take part in this auction
    	var invitedBidder = false;
    	var previousBid = false;
        var currentTime = new Date();

        if (auction.finished || auction.endDateTime <= currentTime) {
            throw new Meteor.Error("This auction has ended. The bid was not registered.");
        }

        if (auction.seller == Meteor.userId()) {
            throw new Meteor.Error("Sorry, you cannot bid on your own auction.");
        }

    	// If it is not a public auction we must determine if the user has been invited to bid
    	if (auction.public == false) {
    		// Check if this user is an invitedBidder
	    	for (var i = 0; i < auction.invitedBidders.length; i++) {
	    		if (auction.invitedBidders[i] == Meteor.userId()) {
	    			invitedBidder = true;
	    			break;
	    		}
	    	}
    	};

    	// Check if user has already made a bid.
    	for (var j = 0; j < auction.bids.length; j++) {
    		if (auction.bids[j].bidder == Meteor.userId()) {
    			previousBid = true;
    		}
    	}


    	if (invitedBidder || auction.public) {
    		if (previousBid) {
    			throw new Meteor.Error("Attempt to insert bid into auction " + aAuctionId + " has failed. User " + Meteor.userId() + " has already made a bid in this auction.");
    		} else {
                var currentDate = new Date();
                bids.insert({auction: aAuctionId, bidder: Meteor.userId(), date: currentDate, selectedOptions: aSelectedOptions, amount: aBidAmount}, function(error, result){
                    if (!error) {
                        return auctions.update({_id:aAuctionId}, {$addToSet: {bids: { bidder : Meteor.userId(), date : currentDate, selectedOptions: aSelectedOptions, amount: aBidAmount}}});
                    } else {
                        throw new Meteor.Error("Failed to insert Bid. This may be due to a server error, please try again in a moment.");
                    }
                });
    		}
    	} else {
    		throw new Meteor.Error("Attempt to insert bid into auction " + aAuctionId + " has failed. User " + Meteor.userId() + " is not invited to bid on this private auction.");
    	}

    },

    /**
    *   @summary Adds a new english auction the auctions collection
    *   @method insertNewEnglishAuction
    *   @param {Object} aAuction This object contains all the data needed to create the auction including; name, description etc.
    *   
    */
    insertNewEnglishAuction: function(aAuction) {
        var auction = aAuction;
        checkAuction(auction);

        var startDateTime = {}; 
        startDateTime = checkStartDateTime(auction.auctionStartDate);

        var endDateTime = {};
        endDateTime = checkEndDateTime(auction.auctionEndDate);
        endDateTime.setSeconds(0,-1);

        return auctions.insert({
            auction_name: auction.auctionName,
            seller: Meteor.userId(),
            auction_description: auction.auctionDescription,
            auction_direction: auction.auctionDirection,
            auction_type: "english",
            auction_options: auction.checkedOptions,
            invitedBidders: [],
            bids: [],
            bidStep: auction.bidStep,
            public: auction.auctionPublic,
            startPrice: auction.startPrice,
            reservePrice: auction.auctionReserve,
            startDateTime: startDateTime,
            endDateTime: endDateTime,
            finished: false
        }, function(error, result) { 
            if (error) {
                console.log('error');
                throw new Meteor.Error("Insert failed.");
            } else {
                Meteor.users.update({ _id: Meteor.userId() }, { $addToSet: { myAuctions: result.valueOf() }});
                Meteor.call('addUsersToAuction', result.valueOf(), auction.invitedBidders);
            }
        });


    },

    /**
    *   @summary Insert a bid into a english auction.
    *   @method insertEnglishBid
    *   @param {Integer} aBidAmount The value the user is bidding.
    *   @param {String} aAuctionId The id of the auction the user is bidding on. This must be an english auction. 
    *   
    */
    insertEnglishBid: function(aBidAmount, aAuctionId) {

        // get the auction associated with aAuctionId
        var auction = auctions.findOne({ _id : aAuctionId });

        // Check if the user has authorisation to take part in this auction
        var invitedBidder = false;

        var currentDate = new Date();
        if (auction.seller == Meteor.userId()) {
            throw new Meteor.Error("Sorry, you cannot bid on your own auction.");
        }

        if (auction.finished || currentDate > auction.endDateTime) {
            throw new Meteor.Error("This auction has ended. The bid was not registered.");
        } 

        
        // If it is not a public auction we must determine if the user has been invited to bid
        if (auction.public == false) {
            // Check if this user is an invitedBidder
            for (var i = 0; i < auction.invitedBidders.length; i++) {
                if (auction.invitedBidders[i] == Meteor.userId()) {
                    invitedBidder = true;
                    break;
                }
            }
        };

        var timeDiff =  (auction.endDateTime.getTime() - currentDate.getTime())/1000;

        if (invitedBidder || auction.public) {
            // Now that authority has been established, check if the bid is higher than current winning bid.

            if (auction.auction_direction == "ascending") {
               if (isHighestBid(aBidAmount, auction.bids)){
                    if (timeDiff < 60) {
                        extendAuctionTime(auction._id, 60);
                    }    
                    bids.insert({auction: aAuctionId, bidder: Meteor.userId(), type: "english", date: new Date(), amount: aBidAmount}, function(error, result){
                        if (!error) {
                            return auctions.update({_id:aAuctionId}, {$addToSet: {bids: { bidder : Meteor.userId(), date : new Date(), amount: aBidAmount}}});
                        } else {
                            throw new Meteor.Error("Failed to insert Bid. This may be due to a server error, please try again in a moment.");
                        }
                    });
                 } else {
                    throw new Meteor.Error("Bid does not exceed current winning bid. Please enter a bid which is greater than the highest bid.");
                } 
            } else if (auction.auction_direction == "descending") {
                if (isLowestBid(aBidAmount, auction.bids)){
                    if (timeDiff < 60) {
                        extendAuctionTime(auction._id, 60);
                    }    
                    bids.insert({auction: aAuctionId, bidder: Meteor.userId(), type: "english", date: new Date(), amount: aBidAmount}, function(error, result){
                        if (!error) {
                            return auctions.update({_id:aAuctionId}, {$addToSet: {bids: { bidder : Meteor.userId(), date : new Date(), amount: aBidAmount}}});
                        } else {
                            throw new Meteor.Error("Failed to insert Bid. This may be due to a server error, please try again in a moment.");
                        }
                    });
                 } else {
                    throw new Meteor.Error("Bid does not exceed current winning bid. Please enter a bid which is less than the lowest bid.");
                }                 
            }
            
        } else {
            throw new Meteor.Error("Attempt to insert bid into auction " + aAuctionId + " has failed. User " + Meteor.userId() + " is not invited to bid on this private auction.");
        }

    },

    /**
    *   @summary Insert a new Dutch Auction into the auctions collection.
    *   @method insertNewDutchAuction
    *   @param {Object} aAuction This object contains all the data for the auction.
    *   
    */
    insertNewDutchAuction: function(aAuction){

        var currentTime = new Date();

        var auction = aAuction;
        checkAuction(auction);

        var startDateTime = {}; 
        startDateTime = checkStartDateTime(auction.auctionStartDate);

        var reductionInterval = [];
        reductionInterval = checkReductionInterval(auction.reductionInterval);

        
        var nextReduceTime = {};
        nextReduceTime = addToDate(currentTime, "day", reductionInterval[0]);
        nextReduceTime = addToDate(nextReduceTime, "hour", reductionInterval[1]);
        nextReduceTime = addToDate(nextReduceTime, "minute", reductionInterval[2]);
        nextReduceTime.setSeconds(0,-1);

        //console.log("AUCTION TYPE: " + auction.auctionType);

        return auctions.insert({
            auction_name: auction.auctionName,
            seller: Meteor.userId(),
            auction_description: auction.auctionDescription,
            auction_direction: auction.auctionDirection,
            auction_type: auction.auctionType,
            auction_options: auction.checkedOptions,
            invitedBidders: [],
            bids: [],
            public: auction.auctionPublic,
            startPrice: auction.startPrice,
            currentPrice: auction.startPrice,
            reservePrice: auction.auctionReserve,
            reductionAmount: auction.reductionAmount,
            startDateTime: startDateTime,
            reductionInterval: reductionInterval,
            nextReduceTime: nextReduceTime,
            winner: null,
            finished: false
        }, function(error, result) { 
            if (error) {
                console.log('error');
                throw new Meteor.Error("Insert failed.");
            } else {
                Meteor.users.update({ _id: Meteor.userId() }, { $addToSet: { myAuctions: result.valueOf() }});
                Meteor.call('addUsersToAuction', result.valueOf(), auction.invitedBidders);
            }
        });
    },

    /**
    *   @summary End the Dutch Auction by buying the item at the current price.
    *   @method insertDutchBid
    *   @param {String} aAuctionId The ID of the dutch auction which is receiving a bid.
    *   
    */
    insertDutchBid: function(aAuctionId) {
        // get the auction associated with aAuctionId
        var auction = auctions.findOne({ _id : aAuctionId });
        var currentPrice = auction.currentPrice; // Get the current price of the auction first as this is the price the user bought at

        // Check if the user has authorisation to take part in this auction
        var invitedBidder = false;

        // Check the auction has not already finished
        if (auction.finished) {
            throw new Meteor.Error("This auction has ended. The bid was not registered.");
        }

        // Check the user isn't the seller
        if (auction.seller == Meteor.userId()) {
            throw new Meteor.Error("Sorry, you cannot bid on your own auction.");
        }

        // If it is not a public auction we must determine if the user has been invited to bid
        if (auction.public == false) {
            // Check if this user is an invitedBidder
            for (var i = 0; i < auction.invitedBidders.length; i++) {
                if (auction.invitedBidders[i] == Meteor.userId()) {
                    invitedBidder = true;
                    break;
                }
            }
        };

        if (invitedBidder || auction.public) {
            if (auction.winner == null) {
                return auctions.update({ _id : aAuctionId }, { $set: { finished: true, winner: { _id: Meteor.userId(), price: currentPrice, date: new Date() } } });
            } else {
                throw new Meteor.Error("Sorry, your bid was too late and another bid won this auction.")
            }
        } else {
            throw new Meteor.Error("Attempt to insert bid into auction " + aAuctionId + " has failed. User " + Meteor.userId() + " is not invited to bid on this private auction.");
        }


    },

    /**
    *   @summary Adds a new second-price auction the auctions collection
    *   @method insertNewSecondPriceAuction
    *   @param {Object} aAuction This object contains all the data needed to create the auction including; name, description etc.
    *   
    */
    insertNewSecondPriceAuction: function(aAuction) {
        // The parameters can be checked securely on the server in this method. (To be added..)
        // e.g if (aReservePrice < 0)
        var auction = aAuction;
        checkAuction(auction);

        var startDateTime = {}; 
        startDateTime = checkStartDateTime(auction.auctionStartDate);

        var endDateTime = {};
        endDateTime = checkEndDateTime(auction.auctionEndDate);
        endDateTime.setSeconds(0,-1);
        return auctions.insert({
           auction_name: auction.auctionName,
           seller: Meteor.userId(),
           auction_description: auction.auctionDescription,
           auction_direction: auction.auctionDirection,
           auction_type: "secondprice",
           auction_options: auction.checkedOptions,
           invitedBidders: [],
           bids: [],
           winner: [],
           images: [],
           public: auction.auctionPublic,
           reservePrice: auction.auctionReserve,
           startDateTime: startDateTime,
           endDateTime: endDateTime,
           finished: false
        }, function(error, result) { 
            if (error) {
                console.log('error');
                throw new Meteor.Error("Insert failed.");
            } else {
                Meteor.users.update({ _id: Meteor.userId() }, { $addToSet: { myAuctions: result.valueOf() }});
                Meteor.call('addUsersToAuction', result.valueOf(), auction.invitedBidders);
            }
        });
    },

    /**
    *   @summary Insert a bid into a second price auction. A user is only allowed one bid in a blind auction.
    *   @method insertSecondPriceBid
    *   @param {Integer} aBidAmount The value the user is bidding.
    *   @param {Array}  aSelectedOptions The options the user has selected to accompany their bid.
    *   @param {String} aAuctionId The id of the auction the user is bidding on. This must be a second-price auction. 
    *   
    */
   insertSecondPriceBid: function(aBidAmount, aSelectedOptions, aAuctionId) {
        // get the auction associated with aAuctionId
        var auction = auctions.findOne({ _id : aAuctionId });
        // Check if the user has authorisation to take part in this auction
        var invitedBidder = false;
        var previousBid = false;

        if (auction.finished) {
            throw new Meteor.Error("This auction has ended. The bid was not registered.");
        }

        if (auction.seller == Meteor.userId()) {
            throw new Meteor.Error("Sorry, you cannot bid on your own auction.");
        }

        // If it is not a public auction we must determine if the user has been invited to bid
        if (auction.public == false) {
            // Check if this user is an invitedBidder
            for (var i = 0; i < auction.invitedBidders.length; i++) {
                if (auction.invitedBidders[i] == Meteor.userId()) {
                    invitedBidder = true;
                    break;
                }
            }
        };

        // Check if user has already made a bid.
        for (var j = 0; j < auction.bids.length; j++) {
            if (auction.bids[j].bidder == Meteor.userId()) {
                previousBid = true;
            }
        }


        if (invitedBidder || auction.public) {
            if (previousBid) {
                throw new Meteor.Error("Attempt to insert bid into auction " + aAuctionId + " has failed. User " + Meteor.userId() + " has already made a bid in this auction.");
            } else {
                var currentDate = new Date();
                bids.insert({auction: aAuctionId, bidder: Meteor.userId(), date: currentDate, selectedOptions: aSelectedOptions, amount: aBidAmount}, function(error, result){
                    if (!error) {
                        return auctions.update({_id:aAuctionId}, {$addToSet: {bids: { bidder : Meteor.userId(), date : currentDate, selectedOptions: aSelectedOptions, amount: aBidAmount}}});
                    } else {
                        throw new Meteor.Error("Failed to insert Bid. This may be due to a server error, please try again in a moment.");
                    }
                });
            }
        } else {
            throw new Meteor.Error("Attempt to insert bid into auction " + aAuctionId + " has failed. User " + Meteor.userId() + " is not invited to bid on this private auction.");
        }

    },

    /**
    *   @summary This method adds an imageId from an image in the database to an auction 
    *   @method addImageToAuction  
    *   @param {String} auction The id of the auction the image is being added to 
    *   @param {String} imageId The id of the image being added.
    *   
    */ 
    addImageToAuction: function(auction, imageId) {
        return auctions.update({ _id : auction }, { $addToSet: { images : imageId } });
    },

    /**
    *   @summary This method adds an imageId from an image in the database to an auction 
    *   @method addUsersToAuction  
    *   @param {String} auction The id of the auction the image is being added to 
    *   @param {String} imageId The id of the image being added.
    *   
    */ 
    addUsersToAuction: function(aAuctionId, aUsers) {
        // Add each user in users to the Auction.
        console.log('aAuctionId: ' + aAuctionId);
        var auctionId = new Mongo.ObjectID(aAuctionId);
        var users = aUsers;
        for (var i = 0; i < users.length; i++) {
            if (auctions.update({_id: auctionId}, { $addToSet: { invitedBidders: users[i] }})) {
                if (Meteor.users.update({_id: users[i]}, { $addToSet: { watchedAuctions: { auction : auctionId } } })) {
                    Meteor.users.update({_id: users[i]}, {$addToSet: { auctionInvites: { auction : auctionId } } });
                }
            }
        }
    }
})

/**************************************************
**************  UTILITY FUNCTIONS *****************   
***************************************************/


function isHighestBid(bidAmount, bids) {
    for (var i = 0; i < bids.length; i++) {
        if (bids[i].amount >= bidAmount) { 
            return false;
        }
    }
    return true;
}

function isLowestBid(bidAmount, bids) {
    for (var i = 0; i < bids.length; i++) {
        if (bids[i].amount <= bidAmount) { 
            return false;
        }
    }
    return true;
}



/**************************************************
**************   CHECK USER INPUTS ****************
***************************************************/
function checkAuction(aAuction) {
    if (aAuction.auctionName == "" || aAuction.auctionName == undefined) {
        throw new Meteor.Error("Auction Name not defined.");
    }
    if (aAuction.auctionDescription == "" || aAuction.auctionDescription == undefined) {
        throw new Meteor.Error("Auction Description not defined.");
    }
    if (aAuction.auctionDirection == undefined) {
        throw new Meteor.Error("Auction Direction not defined.");
    }
    if (aAuction.auctionPublic == undefined) {
        throw new Meteor.Error("Auction Public/Private not set.");
    }

    if (aAuction.auctionReserve <= 0) {
        throw new Meteor.Error("Reserve Price cannot be less than or equal to zero. If you wish to set no Reserve Price please leave it blank.");
    }
    return true;
}

function checkStartDateTime(auctionStartDateTime) {
    var currentTime = new Date();
    if (auctionStartDateTime != undefined) {
        startDateTime = new Date(auctionStartDateTime);
        // Check the user has not set a start time before the currentTime. This is crucial for integrity.
        if (startDateTime < currentTime) {
            startDateTime = new Date(); // if the start time is before the current time then set a new starttime to now.
        }         
    } else {
        startDateTime = new Date(); // if there's no defined startdatetime then start immediately.
    }
    return startDateTime;
}

function checkEndDateTime(auctionEndDateTime) {
    var currentTime = new Date();
    if (auctionEndDateTime != undefined) {
        endDateTime = new Date(auctionEndDateTime);
        if (endDateTime <= currentTime || endDateTime <= startDateTime) {
            throw new Meteor.Error("Invalid End Date/Time. This End Date/Time expires before the auction begins.")
        }
    } else {
        throw new Meteor.Error("End Date and Time not defined. You cannot create an unending Auction.")
    }
    return endDateTime;
}

function checkReductionInterval(reductionInterval) {
    var array = reductionInterval.split('-');

    if (array.length != 3) {
        throw new Meteor.Error("Invalid reduction interval. Please ensure the formatting is correct: " + reductionInterval);
    }

    var intArray = new Array(array.length);
    for (var i = 0; i < array.length; i++) {
        intArray[i] = parseInt(array[i]);
    };

    // Check Day
    if (intArray[0] < 0 || intArray [0] > 31) {
        throw new Meteor.Error("Day interval must be between 0 and 31");
    };

    // Check Hour
    if (intArray[1] < 0 || intArray[1] > 23) {
        throw new Meteor.Error("Hour interval must be between 0 and 31");
    };

    // Check Minute
    if (intArray[2] < 0 || intArray[2] > 59) {
        throw new Meteor.Error("Minute interval must be between 0 and 31");
    };

    if (intArray[0] == 0 && intArray[1] == 0 && intArray[2] < 5) {
        throw new Meteor.Error("You must set a minimum reduction interval of 5 minutes.");
    }
    return intArray;

}



function addToDate(date, unit, quantity) {
  var currentTime = new Date(date); //don't change original date
  switch(unit.toLowerCase()) {
    case 'minute':
        currentTime.setTime(currentTime.getTime() + quantity*60000);
        break;
    case 'hour':
        currentTime.setTime(currentTime.getTime() + quantity*3600000);
        break;
    case 'day':
        currentTime.setDate(currentTime.getDate() + quantity);
        break;
  }
  return currentTime;
}

/**
*   @summary This function extends the end time of an auction specified by auctionId by time 'time'.
*   @method extendAuctionTime  
*   @param {String} auctionId The auction to be extended's id.
*   @param {String} time The amount of time to add to the auction.
*   
*/ 
function extendAuctionTime(auctionId, time) {
    var auction = auctions.findOne({ _id : auctionId });
    
    var auctionEndTime = auction.endDateTime;
    auctionEndTime.setSeconds(auctionEndTime.getSeconds() + time);

    auctions.update({ _id : auctionId }, {$set: { endDateTime : auctionEndTime}})
}






