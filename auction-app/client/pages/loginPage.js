Template.loginPage.helpers({


});

Template.loginPage.events({

	'submit #loginForm': function(event) {
		event.preventDefault();
		var email = $('#loginEmail').val();
		var password = $('#loginPassword').val();
		Meteor.loginWithPassword(email, password, function(err) {
		if (err) {
			alert("Error Logging in: " + err);
			return;
		}
		if (Router.current().route.getName() === 'loginPage') {
			// if we are on the login route, we want to redirect the user
			return Router.go('homePage');
		}
    });
	}

});