Template.profilePage.helpers({

	isOwner: function() {
		return this._id == Meteor.userId();
	},

	formatParagraph: function (str, is_xhtml) {
		console.log('str: ' + str);
 		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
  		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	},

	editBio: function(){
		return Template.instance().editBio.get();
	},

	uploadingImage:function() {
		return Template.instance().uploadingImage.get();
	},

	profileImage: function() {
		return profilePics.findOne({_id: this.profile.picture})
	},

	isProfilePic: function() {
		if (this.profile.picture != null) {
			return true;
		} else {
			return false;
		}
	},

	alreadyFriends: function() {
		var user = Meteor.users.findOne({_id : Meteor.userId()});
		var friends = user.friends;
		var friendRequests = user.friendRequests;

		var alreadyFriends = false;
		for (var i = 0; i < friends.length; i++) {
			if (friends[i]._id == this._id){
				alreadyFriends = true;
				return alreadyFriends;
			}
		}

		for (var j = 0; j < friendRequests.length; j++) {
			if (friendRequests[j].friendId == this._id) {
				alreadyFriends = true;
				return alreadyFriends;
			}
		}
		
	}

});

Template.profilePage.events({

	'change #uploadProfilePic' ( event, template ) {
		template.uploadingImage.set( true );
		var input = document.getElementById('uploadProfilePic');
		//for every file...
		var file = input.files[0];
		profilePics.insert(file, function(err, fileObj) {
			if (err) {
				console.log('err: ' + err);
				template.uploadingImage.set( false );
				Bert.alert( 'Profile Picture Upload Failed' + err.reason, 'warn', 'growl-top-right');
			} else {
				Meteor.call('addProfilePic', fileObj._id, function(error, result){
					if (error) {
						Bert.alert( 'Profile Picture Upload Failed' + error.reason, 'warn', 'growl-top-right');
					} else {
						template.uploadingImage.set( false );
						Bert.alert( 'Picture Successfully Uploaded!', 'success', 'growl-top-right' );
					}
				})

			}
  		});
  	},

  	'click button#editBio': function(event){
  		Template.instance().editBio.set(true);
  	},

  	'click button[data-function="cancelEditBio"]': function(event) {
  		Template.instance().editBio.set(false);
  	},

  	'click button[data-function="submitEditBio"]': function(event) {
  		var newBio = $('#newBio').val();
  		var template = Template.instance();
  		
  		Meteor.call('updateUserBio', newBio, function(error, result) {
  			if (error) {
				Bert.alert( 'Error updating Bio: ' + error.reason, 'warn', 'growl-top-right');
  			} else {
				Bert.alert( 'Bio Updated!', 'success', 'growl-top-right' );
				template.editBio.set(false);
  			}
  		});
  	},

  	'click button[data-function="addFriend"]': function(event) {
  		Meteor.call('addFriend', this._id, function(error, result) {
  			if (error) {
 				Bert.alert( 'Friend Request Failed: ' + error.reason, 'warn', 'growl-top-right');
  			} else {
  				Bert.alert( 'Friend Request Sent!', 'success', 'growl-top-right' );
  			}
  			
  		})
  	},

  	'click button[data-function="removeFriend"]': function(event) {
		var friendId = this._id;
		Meteor.call('removeFriend', friendId, function(error, result) {
			if (!error) {
				Bert.alert( 'Friend Removed', 'success', 'growl-top-right' );
			}

		});
	}

});

Template.profilePage.onCreated( () => {
	Template.instance().uploadingImage = new ReactiveVar( false );
	Template.instance().editBio = new ReactiveVar( false );
});