Template.viewAuctions.helpers({

    watchedAuctions: function() {
        var user = Meteor.users.findOne({_id:Meteor.userId()}, {fields: {watchedAuctions: 1} });
        if (user && user.watchedAuctions) {
            return user.watchedAuctions;
        } else {
            return
        }
    },

	auction: function() {
		return auctions.findOne({_id: this.auction});
	},

    timeRemaining: function() {
        var end = this.endDateTime;
        var serverTime = new Date(TimeSync.serverTime(null, 1000));
        var duration = moment.duration(end - serverTime, 'milliseconds');
        return duration.days() + ":" + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
    },

    numberOfBids: function() {
        return this.bids.length;
    },

    sellerCompany: function() {
        var seller = Meteor.users.findOne({_id : this.seller});
        return seller.company;
    }

});

Template.viewAuctions.events({

    'click tbody > tr > td': function(event){
        var auctionId = this._id;
        hex = auctionId.valueOf();
        document.location.href = "/"+ this.auction_type + "/"+hex;
        
    }

});