Template.viewPublicAuctions.helpers({

	publicAuction: function(){
		var sortData = Template.instance().sort.get();
		if (sortData) {
			var query = {};
			query[sortData] = 1;
		}
		return auctions.find({ public: true }, {sort: query});
	},

	numberOfBids: function() {
		return this.bids.length;
	},

	sellerCompany: function() {
		var seller = Meteor.users.findOne({_id : this.seller});
		console.log('this.selleR: '+ this.seller);
		return seller.company;
	},

	hexId: function() {
		var auctionId = this._id;
        hex = auctionId.valueOf();
		return hex;
	}

});

Template.viewPublicAuctions.events({

	'click thead > tr > th': function(event) {
		console.log("click");
		Template.instance().sort.set($(event.currentTarget).attr("data-header"));
	}

});

Template.viewPublicAuctions.onCreated(() => {
	Template.instance().sort = new ReactiveVar("auction_name");
});