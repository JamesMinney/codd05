Template.myAuctions.helpers({

	sellerCompany: function() {
		var seller = Meteor.users.findOne({_id : this.seller});
		console.log('this.selleR: '+ this.seller);
		return seller.company;
	},

	numberOfBids: function() {
		return this.bids.length;
	},

	hexId: function() {
		var auctionId = this._id;
        hex = auctionId.valueOf();
		return hex;
	}

});