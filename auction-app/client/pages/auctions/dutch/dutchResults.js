Template.dutchResults.helpers({

	isSeller: function() {
		return Meteor.userId() == this.seller;
	},
	
	isUserWinner: function() {
		if (this.winner._id == Meteor.userId()) {
			return true;
		} else {
			return false;
		}
	}

})