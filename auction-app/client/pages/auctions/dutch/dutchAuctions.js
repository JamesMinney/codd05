Template.dutchAuction.helpers({

	isSeller: function() {
		return Meteor.userId() == this.seller;
	},
	
	sellerCompany: function() {
		var user = Meteor.users.findOne({ _id : this.seller });
		return user.company;
	},

	timeRemaining: function() {
		var end = this.nextReduceTime;
		var serverTime = new Date(TimeSync.serverTime(null, 1000));
		var duration = moment.duration(end - serverTime, 'milliseconds');

		return duration.days() + ":" + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
	},

	nextPrice: function() {
		if (this.auction_direction == "ascending") {
			return (this.currentPrice - this.reductionAmount);
		} else if (this.auction_direction == "descending") {
			return (parseInt(this.currentPrice) + parseInt(this.reductionAmount));
		}
	},

	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var options = this.auction_options;
		var formattedArray = []; // The formatted array that will be returned

		if (options) {
			size = 4; // How many items in the row
			while (options.length > size) {
				formattedArray.push({ row: options.slice(0, size)});
				options = options.slice(size);
			}
	        formattedArray.push({row: options});
	    }
	    return formattedArray;
	}

});

Template.dutchAuction.events({

	'click button[data-function="buy-at-price"]': function(event) {
		Meteor.call('insertDutchBid', this._id, function(error, result) {
			if (!error) {
				Bert.alert( 'Bid successfully made!', 'success', 'growl-top-right' );
			} else {
				Bert.alert( error, 'danger', 'growl-top-right' );
			}
		})
	},

	'click #auctionReview': function(event) {
		var auctionId = this._id;
        hex = auctionId.valueOf();
        document.location.href = "/dutch/results/"+hex;
	}

})