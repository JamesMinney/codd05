Template.secondpriceResults.helpers({
	
	isSeller: function() {
		return Meteor.userId() == this.seller;
	},

	isUserWinner: function() {
		for (var i = 0; i < this.winner.length; i++) {
			if (this.winner[i]._id == Meteor.userId()) {
				return true;
			} else {
				return false;
			}
		}
	}

});