Template.secondprice.helpers({

	noBidMade: function() {
		if (bids.findOne({auction: this._id, bidder: Meteor.userId()})) {
			return false;
		} else {
			return true;
		}
	},

	timeRemaining: function() {
		var end = this.endDateTime;
		var serverTime = new Date(TimeSync.serverTime(null, 1000));
		var duration = moment.duration(end - serverTime, 'milliseconds');

		return duration.days() + ":" + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
	},

	isSeller: function() {
		return Meteor.userId() == this.seller;
	},

	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var options = this.auction_options;
		var formattedArray = []; // The formatted array that will be returned

		if (options) {
			size = 4; // How many items in the row
			while (options.length > size) {
				formattedArray.push({ row: options.slice(0, size)});
				options = options.slice(size);
			}
	        formattedArray.push({row: options});
	    }
	    return formattedArray;
	}

});

Template.secondprice.events({
	

	'submit .insert-secondprice-bid': function(event) {
		event.preventDefault();

		var bidAmount = $("#blind-bid-amount").val();

		var selectedOptions = [];
		// Loop through all checked options and push them to the array.
		$('input[name="selectedOptions"]:checked').each(function() {
			selectedOptions.push($(this).val());
		});

		Meteor.call("insertSecondPriceBid", bidAmount, selectedOptions, this._id, function(error, result) {
			if (!error) {
				Bert.alert( 'Bid successfully made!', 'success', 'growl-top-right' );
			} else {
				Bert.alert( error, 'danger', 'growl-top-right' );
			}
		});
	},

	'click #auctionReview': function(event) {
		var auctionId = this._id;
        hex = auctionId.valueOf();
        document.location.href = "/secondprice/results/"+hex;
	}

});