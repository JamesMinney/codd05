Template.englishResults.helpers({
	
	isSeller: function() {
		return Meteor.userId() == this.seller;
	},

	auctionBid: function() {
		return bids.find({"auction": this._id})
	},

	winningBid: function() {
		
		var highestBidder = false;
		var auctionBids = bids.find({"auction": this._id});

		if (this.auction_direction == "ascending") {
			var highestBid = 0;
			auctionBids.forEach(function(bid) {
				if (parseInt(bid.amount) > parseInt(highestBid)) {
					highestBid = bid.amount;
					if (bid.bidder != undefined) {
						if (bid.bidder == Meteor.userId()) {
							highestBidder = true;
						} else {
							highestBidder = false;
						}
					} else {
						highestBidder = false;
					}
				}
			})
		} else if (this.auction_direction == "descending") {
			var highestBid = this.startPrice;
			console.log(this.startPrice)
			auctionBids.forEach(function(bid) {
				if (parseInt(bid.amount) < parseInt(highestBid)) {
					highestBid = bid.amount;
					if (bid.bidder != undefined) {
						if (bid.bidder == Meteor.userId()) {
							highestBidder = true;
						} else {
							highestBidder = false;
						}
					} else {
						highestBidder = false;
					}
				}
			})
		}
		var temp = new Object();
		temp["highestBid"] = highestBid;
		temp["highestBidder"] = highestBidder;
		return temp;
	},

})