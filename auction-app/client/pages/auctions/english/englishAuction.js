Template.englishAuction.helpers({
	
	/**
	*	@summary HELPER: This function is used to alter the DOM and prevent the user 
	*			making a bid in a blind auction if they have already submitted a bid.
	*			This helper is non-critical to security as even if the user forces this to return false
	*			the user can still not insert multiple bids.
	*/
	ascending: function() {
		return this.auction_direction == "ascending";
	},

	descending: function() {
		return this.auction_direction == "descending";
	},

	sellerCompany: function() {
		var user = Meteor.users.findOne({ _id : this.seller });
		return user.company;
	},

	noBidMade: function() {
		if (bids.findOne({auction: this._id, bidder: Meteor.userId()})) {
			return false;
		} else {
			return true;
		}
	},

	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var options = this.auction_options;
		var formattedArray = []; // The formatted array that will be returned

		if (options) {
			size = 4; // How many items in the row
			while (options.length > size) {
				formattedArray.push({ row: options.slice(0, size)});
				options = options.slice(size);
			}
	        formattedArray.push({row: options});
	    }
	    return formattedArray;
	},

	winningBid: function() {
		
		var highestBidder = false;
		var auctionBids = bids.find({"auction": this._id});

		if (this.auction_direction == "ascending") {
			var highestBid = 0;
			auctionBids.forEach(function(bid) {
				if (parseInt(bid.amount) > parseInt(highestBid)) {
					highestBid = bid.amount;
					if (bid.bidder != undefined) {
						if (bid.bidder == Meteor.userId()) {
							highestBidder = true;
						} else {
							highestBidder = false;
						}
					} else {
						highestBidder = false;
					}
				}
			})
		} else if (this.auction_direction == "descending") {
			var highestBid = this.startPrice;
			auctionBids.forEach(function(bid) {
				if (parseInt(bid.amount) < parseInt(highestBid)) {
					highestBid = bid.amount;
					if (bid.bidder != undefined) {
						if (bid.bidder == Meteor.userId()) {
							highestBidder = true;
							console.log('highestBidder: True');
						} else {
							highestBidder = false;
						}
					} else {
						console.log('bid.bidder is undefined')
						highestBidder = false;
					}
				}
			})
		}
		var temp = new Object();
		temp["highestBid"] = highestBid;
		temp["highestBidder"] = highestBidder;
		return temp;
	},

	isSeller: function() {
		return Meteor.userId() == this.seller;
	}

});

Template.englishAuction.events({
	
	'submit .insert-blind-bid': function(event) {
		event.preventDefault();

		var bidAmount = $("#blind-bid-amount").val();
		
		Meteor.call("insertEnglishBid", bidAmount, this._id, function(error, result) {
			if (!error) {
				Bert.alert( 'Bid successfully made!', 'success', 'growl-top-right' );
			} else {
				Bert.alert( error, 'danger', 'growl-top-right' );
			}
		});
	},

	'click #auctionReview': function(event) {
		var auctionId = this._id;
        hex = auctionId.valueOf();
        document.location.href = "/english/results/"+hex;
	}

});