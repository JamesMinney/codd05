Template.registerPage.helpers({

	industrySector: function() {
		return sectors.find({});
	}

});


Template.registerPage.events({

	'click #submit-registration': function(evt) {

		// Get the data from all the fields..
		var firstname = $('#input-firstname').val();
		var surname = $('#input-surname').val();
		var email = $('input[type=email]').val();
		var company = $('#input-company').val();
		var sector = $('#input-sector :selected').val();
		var password = $('#input-password').val();
		var retypePassword = $('#input-retype-password').val();


		Meteor.call('createNewUser', firstname, surname, email, sector, company, password, function(err) {
			if (err) {
				Bert.alert( 'Registration Error: ' + err, 'warn', 'growl-top-right' );
			} else {
				Bert.alert( 'Auction Successfully Created!', 'success', 'growl-top-right' );
				document.location.href = '/homePage';
			}
		});

	}


});