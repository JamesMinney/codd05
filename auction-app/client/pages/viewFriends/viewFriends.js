Template.viewFriends.helpers({ 

	emptyStar:function() {
		return (5-this.assignedGrade);
	},

	userList: function() {
		return Meteor.users.find({});
	}, 

	indexZero: function(index) {
		return index == 0;
	},

	friend: function() {
		var user = Meteor.users.findOne({_id:Meteor.userId()}, {fields: {friends: 1} });
		if (user && user.friends) {
    		return user.friends;
    	} else {
    		return
    	}
	},

	friendDocument: function() {
		return Meteor.users.findOne({_id:this._id});
	},

	friendSince: function(friendDate) {
		var end = friendDate;
        var serverTime = new Date(TimeSync.serverTime(null, 1000));
        var duration = moment.duration(end - serverTime, 'milliseconds');
        return duration.days() + ":" + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
	},

	loop: function(startIndex,loopSize) {
		var countArr = [];
	    for (var i=parseInt(startIndex); i<loopSize; i++){
	      countArr.push({friendId: this._id, value: (i+1)});
	    }
	    return countArr;
	}
	
});

Template.viewFriends.events({

	'click #add-friend': function(event) {
		var selectedFriend = $('#select-friend').val();
		Meteor.call('addFriend', selectedFriend, function(error, result) {
			if (!error) {
				Bert.alert( 'Friend Request Sent!', 'success', 'growl-top-right' );
			}
		});
	},

	'click .glyphicon': function(event) {
		var newValue = $(event.currentTarget).attr('data-value');
		console.log("friendId: " + this.friendId);
		Meteor.call('changeAssignedGrade', this.friendId, newValue, function(error, result) {

		});
	},

	'click button[data-function="removeFriend"]': function(event) {
		var friendId = this._id;
		Meteor.call('removeFriend', friendId, function(error, result) {
			if (!error) {
				Bert.alert( 'Friend Removed', 'success', 'growl-top-right' );
			}

		});
	}

});

Template.addFriend.helpers({

	searchResults: function() {
		var searchTerm = Template.instance().searchTerm.get();
		var users = Meteor.users.find({ $or: [
				{"company": searchTerm},
				{"emails.address": searchTerm}
			]
		})
		var resultLength = 0;
		users.forEach(function(user) {
			resultLength++;
		})

		var temp = new Object();
		if (resultLength > 0) {
			temp.hasResults = true;
		} else {
			temp.hasResults = false;
		}
		console.log(temp.hasResults)
		temp.results = users;
		return temp;
	}

})

Template.addFriend.events({

	'click button[data-function="searchUsers"]': function(event){
		var searchTerm = $('#friendSearchBar').val();
		Template.instance().searchTerm.set(searchTerm);
	},

	'keypress #friendSearchBar': function(event){
		if (event.which === 13) {
			var searchTerm = $('#friendSearchBar').val();
			Template.instance().searchTerm.set(searchTerm);
		}
	}
})

Template.addFriend.onCreated( () => {
	Template.instance().searchTerm = new ReactiveVar("");
})
