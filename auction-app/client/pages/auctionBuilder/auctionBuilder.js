Template.auctionBuilder.helpers({
	
	selectedAuctionTemplate: function(target) {
		return Template.instance().auctionTemplate.get() == target;
	},

	selectedAuctionType: function(target) {
		return Template.instance().auctionType.get() == target;
	}

});

Template.auctionBuilder.events({
	
	'click [data-action="auction-template"]': function(event) {
		Template.instance().auctionTemplate.set($(event.currentTarget).attr("data-target"));
		var targetSection = $('#auctionTemplateAnchor');
		$('html,body').animate({scrollTop: targetSection.offset().top},'slow');
	},

	'click [data-action="set-auction-type"]': function(event) {

		$('li[data-action="set-auction-type"]').removeClass('active');
		$(event.currentTarget).addClass('active');

		Template.instance().auctionType.set($(event.currentTarget).attr("data-value"));
	},

	/*
	*	This event fires when the Create Auction button is clicked. The purpose of this function is take
	*	all of the data entered into the auction builder by the user and pass the data to the correct
	*	meteor method for building the auction	
	*/
	'click #abCreateAuction': function(event) {

		// These are general fields which are used in every auction.
		var auctionName = $('#abAuctionName').val();
		var auctionDescription = $('#abAuctionDescription').val();
		var checkedOptions = []; // Array to hold all the checkedOptions.
		var invitedBidders = []; // Array to hold all friends invited to bid on the auction.
		var auctionPublic;
		var auctionStartDate = {};
		var auctionEndDate = {};
		var auctionDirection = $('input[name="abAuctionDirection"]:checked').val();
		var bidStep = $('#abBidStep').val();


		// Loop through all checked options and push them to the array.
		$('input[name="abOptionCheckbox"]:checked').each(function() {
			checkedOptions.push($(this).val());
		});

		// Loop through all checked friends and push their ID to the array.
		$('input[name="abFriendCheckbox"]:checked').each(function() {
			invitedBidders.push($(this).val());
		})

		//Set public to either true or false.
		if ($('input[name="abPublic"]:checked').val() == "public") {
			auctionPublic = true;
		} else {
			auctionPublic = false;
		}

		// If the Auction should start immediately, don't set start time and we'll
		// create this on the server.
		if ($('input[name="abAuctionStart"]:checked').val() == "immediately") {
			auctionStartDate = {}; // undefined, we'll set this on the server.
		} else if ($('input[name="abAuctionStart"]:checked').val() == "atDateTime")  {
			auctionStartDate = $('#abStartDate').val() + ':' + $('#abStartTime').val();
		}

		// Set the auction end date/time.
		auctionEndDate = $('#abEndDate').val() + ':' + $('#abEndTime').val();

		var auction = new Object();
		auction.auctionName = auctionName;
		auction.auctionDescription = auctionDescription;
		auction.checkedOptions = checkedOptions;
		auction.invitedBidders = invitedBidders;
		auction.auctionPublic = auctionPublic;
		auction.auctionStartDate = auctionStartDate;
		auction.auctionEndDate = auctionEndDate;
		auction.auctionDirection = auctionDirection;
		auction.bidStep = bidStep;

		// Determine the auction type in order to find specfic parameters and 
		// call the correct server method.
		var auctionType = Template.instance().auctionType.get();
		auction.auctionType = auctionType;
		switch(auctionType) {
			case "blind":
				var auctionReserve = $('#abReservePrice').val();
				var startPrice = $('#abStartPrice').val();
				auction.auctionReserve = auctionReserve;
				auction.startPrice = startPrice;

				// Insert the new auction and run the callback function with the result.
				Meteor.call("insertNewBlindAuction", auction, function(error, result) {
					if (error) {
						alert('Error inserting new Auction: ' + error);
					} else {
						var input = document.getElementById('abImageFiles');
						if (uploadFiles(result, input.files)) {
							Bert.alert( 'Auction Successfully Created!', 'success', 'growl-top-right' );
							// Delay for 4 seconds while user reads Bert Alert.
							setTimeout(function() {
								document.location.href = '/'+auctionType+'/'+result;
							}, 4000);
						}

						
					}
				});
				break;
			case "english":
				var auctionReserve = $('#abReservePrice').val();
				var startPrice = $('#abStartPrice').val();
				auction.auctionReserve = auctionReserve;
				auction.startPrice = startPrice;
				Meteor.call("insertNewEnglishAuction", auction, function(error, result) {
					if (error) {
						alert('Error inserting new Auction: ' + error);
					} else {
						var input = document.getElementById('abImageFiles');
						if (uploadFiles(result, input.files)) {
							Bert.alert( 'Auction Successfully Created!', 'success', 'growl-top-right' );
							setTimeout(function() {
								document.location.href = '/'+auctionType+'/'+result;
							}, 4000);
						}
					}
				});
				break;
			case "dutch":
				var auctionReserve = $('#abReservePrice').val();
				var startPrice = $('#abStartPrice').val();
				var intervalDay = $('#abReductionIntervalDays').val();
				var intervalHours = $('#abReductionIntervalHours').val();
				var intervalMinutes = $('#abReductionIntervalMinutes').val();
				var reductionAmount = $('#abReductionAmount').val();

				auction.auctionReserve = auctionReserve;
				auction.startPrice = startPrice;
				auction.reductionInterval = intervalDay + "-" + intervalHours + "-" + intervalMinutes;
				auction.reductionAmount = reductionAmount;

				Meteor.call("insertNewDutchAuction", auction, function(error, result) {
					if (error) {
						alert("Error inserting new Auction: " + error);
					} else {
						var input = document.getElementById('abImageFiles');
						if (uploadFiles(result, input.files)) {
							Bert.alert( 'Auction Successfully Created!', 'success', 'growl-top-right' );
							setTimeout(function() {
								document.location.href = '/'+auctionType+'/'+result;
							}, 4000);
						}
					}
				});
				break;
			case "secondprice":
				var auctionReserve = $('#abReservePrice').val();
				var startPrice = $('#abStartPrice').val();
				auction.auctionReserve = auctionReserve;
				auction.startPrice = startPrice;
				//Meteor.call("insertNewBlindAuction", auctionName, auctionDescription, auctionDirection, auctionPublic, auctionReserve, auctionStartDate, auctionEndDate, bidStep, invitedBidders, function(error, result) {
				Meteor.call("insertNewSecondPriceAuction", auction, function(error, result) {
					if (error) {
						alert('Error inserting new Auction: ' + error);
					} else {
						var input = document.getElementById('abImageFiles');
						if (uploadFiles(result, input.files)) {
							Bert.alert( 'Auction Successfully Created!', 'success', 'growl-top-right' );
							setTimeout(function() {
								document.location.href = '/'+auctionType+'/'+result;
							}, 4000);
						}
					}
				});
				break;
			default:
				alert("No Auction Type Specified!");
		}

	}

});


Template.auctionBuilder.onCreated( () => {
	Template.instance().auctionTemplate = new ReactiveVar();
	Template.instance().auctionType = new ReactiveVar("blind");
	
})

function uploadFiles(auction, files) {
	var noError = true;
	for (var i = 0; i < files.length; i++) {
		var newFile = new FS.File(files[i]);
     	newFile.metadata = {auctionId: auction};
		images.insert(newFile, function (err, fileObj) {
			if (err) {
				noError = false;
			}
		});
	}
	return noError;
}