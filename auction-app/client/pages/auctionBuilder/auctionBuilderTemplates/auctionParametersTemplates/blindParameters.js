Template.blindParameters.helpers({
	
	setStartDateTime: function() {
		console.log("Template.instance().setStartDateTime: " +Template.instance().setStartDateTime);
		return Template.instance().setStartDateTime.get();
	}
})

Template.blindParameters.events({

	'click [name="abAuctionStart"]': function(event) {
		if ($(event.currentTarget).val() == "immediately") {
			Template.instance().setStartDateTime.set(false);
		} else if ($(event.currentTarget).val() == "atDateTime") {
			Template.instance().setStartDateTime.set(true);
		}
	}

})

Template.blindParameters.onCreated( () => {
	Template.instance().setStartDateTime = new ReactiveVar(false);
})