Template.secondPriceParameters.helpers({
	
	setStartDateTime: function() {
		return Template.instance().setStartDateTime.get();
	}
})

Template.secondPriceParameters.events({

	'click [name="abAuctionStart"]': function(event) {
		if ($(event.currentTarget).val() == "immediately") {
			Template.instance().setStartDateTime.set(false);
		} else if ($(event.currentTarget).val() == "atDateTime") {
			Template.instance().setStartDateTime.set(true);
		}
	}

})

Template.secondPriceParameters.onCreated( () => {
	Template.instance().setStartDateTime = new ReactiveVar(false);
})