Template.englishParameters.helpers({
	
	setStartDateTime: function() {
		return Template.instance().setStartDateTime.get();
	}
})

Template.englishParameters.events({

	'click [name="abAuctionStart"]': function(event) {
		if ($(event.currentTarget).val() == "immediate") {
			Template.instance().setStartDateTime.set(false);
		} else if ($(event.currentTarget).val() == "atDateTime") {
			Template.instance().setStartDateTime.set(true);
		}
	}

})

Template.englishParameters.onCreated( () => {
	Template.instance().setStartDateTime = new ReactiveVar(false);
})