Template.customAuctionTemplate.helpers({
	
	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var customAuctionOptions = Session.get("customAuctionOptions");
		var formattedArray = []; // The formatted array that will be returned

		if (customAuctionOptions) {
			size = 4; // How many items in the row
			while (customAuctionOptions.length > size) {
				formattedArray.push({ row: customAuctionOptions.slice(0, size)});
				customAuctionOptions = customAuctionOptions.slice(size);
			}
	        formattedArray.push({row: customAuctionOptions});
	    }
	    return formattedArray;
	}

});

Template.customAuctionTemplate.events({
	
	'submit #add-cleaning-option': function(event) {
		event.preventDefault();
		var customAuctionOptions = Session.get("customAuctionOptions");
		var checkedValues = []
		$('input[name=cleaningOptionCheckboxes]').each(function() {
			checkedValues.push($(this).val().toLowerCase().replace(/ /g,''));
		})

		//var newOption = $('#cleaning-option').val();
		if (checkedValues.indexOf($('#cleaning-option').val().toLowerCase().replace(/ /g,'')) == -1) {
			customAuctionOptions.push($('#cleaning-option').val());
			Session.set("customAuctionOptions", customAuctionOptions);
		} else {
			Bert.alert( "Option already exists!", 'danger', 'growl-top-right' );
		}
		$('#cleaning-option').val("");
	}

});


Template.customAuctionTemplate.rendered = function() {

	var customAuctionOptions = [];
	Session.set("customAuctionOptions", customAuctionOptions);

}