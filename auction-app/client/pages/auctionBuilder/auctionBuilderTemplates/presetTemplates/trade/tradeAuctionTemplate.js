Template.tradeAuctionTemplate.helpers({
	
	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var tradeAuctionOptions = Session.get("tradeAuctionOptions");
		var formattedArray = []; // The formatted array that will be returned

		if (tradeAuctionOptions) {
			size = 4; // How many items in the row
			while (tradeAuctionOptions.length > size) {
				formattedArray.push({ row: tradeAuctionOptions.slice(0, size)});
				tradeAuctionOptions = tradeAuctionOptions.slice(size);
			}
	        formattedArray.push({row: tradeAuctionOptions});
	    }
	    return formattedArray;
	}

});

Template.tradeAuctionTemplate.events({
	
	'submit #add-cleaning-option': function(event) {
		event.preventDefault();
		var tradeAuctionOptions = Session.get("tradeAuctionOptions");
		var checkedValues = []
		$('input[name=cleaningOptionCheckboxes]').each(function() {
			checkedValues.push($(this).val().toLowerCase().replace(/ /g,''));
		})

		//var newOption = $('#cleaning-option').val();
		if (checkedValues.indexOf($('#cleaning-option').val().toLowerCase().replace(/ /g,'')) == -1) {
			tradeAuctionOptions.push($('#cleaning-option').val());
			Session.set("tradeAuctionOptions", tradeAuctionOptions);
		} else {
			Bert.alert( "Option already exists!", 'danger', 'growl-top-right' );
		}
		$('#cleaning-option').val("");
	}

});

Template.tradeAuctionTemplate.rendered = function() {

	var tradeAuctionOptions = ["Carpentry", "Plumbing", "Electrician", "Painting", "Plastering"];
	Session.set("tradeAuctionOptions", tradeAuctionOptions);

}