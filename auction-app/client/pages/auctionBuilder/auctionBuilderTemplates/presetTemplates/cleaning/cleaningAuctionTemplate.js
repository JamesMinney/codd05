Template.cleaningAuctionTemplate.helpers({
	
	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var cleaningAuctionOptions = Session.get("cleaningAuctionOptions");
		var formattedArray = []; // The formatted array that will be returned

		if (cleaningAuctionOptions) {
			size = 4; // How many items in the row
			while (cleaningAuctionOptions.length > size) {
				formattedArray.push({ row: cleaningAuctionOptions.slice(0, size)});
				cleaningAuctionOptions = cleaningAuctionOptions.slice(size);
			}
	        formattedArray.push({row: cleaningAuctionOptions});
	    }
	    return formattedArray;
	}

});

Template.cleaningAuctionTemplate.events({
	
	'submit #add-cleaning-option': function(event) {
		event.preventDefault();
		var cleaningAuctionOptions = Session.get("cleaningAuctionOptions");
		var checkedValues = []
		$('input[name=cleaningOptionCheckboxes]').each(function() {
			checkedValues.push($(this).val().toLowerCase().replace(/ /g,''));
		})

		//var newOption = $('#cleaning-option').val();
		if (checkedValues.indexOf($('#cleaning-option').val().toLowerCase().replace(/ /g,'')) == -1) {
			cleaningAuctionOptions.push($('#cleaning-option').val());
			Session.set("cleaningAuctionOptions", cleaningAuctionOptions);
		} else {
			Bert.alert( "Option already exists!", 'danger', 'growl-top-right' );
		}
		$('#cleaning-option').val("");
	}

});


Template.cleaningAuctionTemplate.rendered = function() {

	var cleaningAuctionOptions = ["Windows", "Floors", "Ceilings", "Polish", "Refuse Disposal", "options", "more options", "yet more"];
	Session.set("cleaningAuctionOptions", cleaningAuctionOptions);

}