Template.softwareAuctionTemplate.helpers({
	
	auctionOption: function() {
		// Due to formatting this helper must return a list of rows
		var softwareAuctionOptions = Session.get("softwareAuctionOptions");
		var formattedArray = []; // The formatted array that will be returned

		if (softwareAuctionOptions) {
			size = 4; // How many items in the row
			while (softwareAuctionOptions.length > size) {
				formattedArray.push({ row: softwareAuctionOptions.slice(0, size)});
				softwareAuctionOptions = softwareAuctionOptions.slice(size);
			}
	        formattedArray.push({row: softwareAuctionOptions});
	    }
	    return formattedArray;
	}

});

Template.softwareAuctionTemplate.events({
	
	'submit #add-cleaning-option': function(event) {
		event.preventDefault();
		var softwareAuctionOptions = Session.get("softwareAuctionOptions");
		var checkedValues = []
		$('input[name=cleaningOptionCheckboxes]').each(function() {
			checkedValues.push($(this).val().toLowerCase().replace(/ /g,''));
		})

		//var newOption = $('#cleaning-option').val();
		if (checkedValues.indexOf($('#cleaning-option').val().toLowerCase().replace(/ /g,'')) == -1) {
			softwareAuctionOptions.push($('#cleaning-option').val());
			Session.set("softwareAuctionOptions", softwareAuctionOptions);
		} else {
			Bert.alert( "Option already exists!", 'danger', 'growl-top-right' );
		}
		$('#cleaning-option').val("");
	}

});

Template.softwareAuctionTemplate.rendered = function() {

	var softwareAuctionOptions = ["Java", "C", "Front-End", "Back-End", "Web Dev"];
	Session.set("softwareAuctionOptions", softwareAuctionOptions);

}