Template.abInviteFriendsPanel.helpers({
	
	friend: function() {
		// Due to formatting this helper must return a list of rows
		var user = Meteor.users.findOne({ _id : Meteor.userId()});
		var formattedArray = []; // The formatted array that will be returned
		console.log("user.friends: " + user.friends);
		if (user && user.friends) {
			var friends = user.friends;
			size = 4; // How many items in the row
			while (friends.length > size) {
				formattedArray.push({ row: friends.slice(0, size)});
				friends = friends.slice(size);
			}
	        formattedArray.push({row: friends});
	    }
	    return formattedArray;
	},

	friendDocument: function() {
		return Meteor.users.findOne({_id:this._id});
	}

});

Template.abInviteFriendsPanel.onCreated(function() {
	this.subscribe('userList');
	this.subscribe('userData');
});