Template.imageRibbon.helpers({

	auctionImages: function(auctionId) {
		return images.find({"metadata.auctionId": auctionId});
	}

});

Template.imageRibbon.events({

	'click .ribbonThumbnail': function(event) {
		Session.set("activeThumbnail", this._id);
	}

});

Template.imageRibbon.onCreated(function () {

	this.subscribe('auctionImageData');
});