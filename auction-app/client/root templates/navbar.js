Template.navbar.helpers({
	userEmail: function() {
		return Meteor.user().emails[0].address;
	},

	unreadNotificationsLength: function() {
		if (Meteor.userId()) {
			var user = Meteor.users.findOne({_id:Meteor.userId()});
			console.log('user.company: ' + user.company);
			var count = 0;
			for (var i = 0; i < user.friendRequests.length; i++) {
				if (user.friendRequests[i].unread == true) {
					count++;
				}
			}
			count = count + user.auctionInvites.length;
			return count;
		}
	},

	unreadNotification: function() {
		return Meteor.users.find({_id: Meteor.userId()}, {fields: {notifications:1}})
	},

	isUnread: function() {
		return this.unread;
	},

	auctionData: function() {
		return auctions.findOne({_id: this.auction});
	},

	hexId: function() {
		var auctionId = this._id;
        hex = auctionId.valueOf();
		return hex;
	}

});

Template.navbar.events({

	'click button[data-function="accept-friend"]': function(event) {
		event.stopPropagation();
		Meteor.call("handleFriendRequest", this, "accept");
	},

	'click button[data-function="reject-friend"]': function(event) {
		event.stopPropagation();
		Meteor.call("handleFriendRequest", this, "reject");
	},

	'click button[data-function="go-to-auction"]': function(event){
		event.stopPropagation();
		console.log("this.auction: " + this.auction);
		var auction = auctions.findOne({_id: this.auction});
		Meteor.call('markAsRead', this, "auctionInvite", function(error, result) {
			
		});
		var hex = auction._id.valueOf();
		document.location.href = "/"+auction.auction_type+"/"+hex;

	},

	'click button[data-function="dismiss"]': function(event){
		Meteor.call('markAsRead', this, "auctionInvite");
	},

	'click a#logout': function(event) {
		event.preventDefault();
		Meteor.logout(function(error) {
			if (!error) {
				document.location.href = "/homePage"
			}
		})
	}

})

Template.navbar.onCreated(function () {

	this.subscribe('userList');
	this.subscribe('publicAuctions');
	this.subscribe('invitedBidderAuctions'),
	this.subscribe('sellerAuctions')
});