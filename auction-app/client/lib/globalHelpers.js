/*
*	Global Helpers
* 	
*	These helpers are added to every template and therefore can be used in any template.
*/
Template.registerHelper( 'formatDate', ( aDate ) => {
	if (aDate == undefined) {
		return "N/A";
	} else {
		date = new Date(aDate);
		return date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
	}
});

Template.registerHelper( 'formatDateTime', (aDateTime) => {
	if (aDateTime == undefined) {
		return "N/A";
	} else {
		date = new Date(aDateTime);
		return date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	}
})

Template.registerHelper( 'getCompany', (userId) => {
	var user = Meteor.users.findOne({ _id : userId });
	return user.company;	
})

Template.registerHelper( 'getUserId', () => {
	return Meteor.userId();
})	

Template.registerHelper( 'capitaliseFirstLetter', (editString) => {
	return editString.replace(/\w\S*/g, function(txt){
		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
	});
})

Template.registerHelper('timeRemaining', (end) => {
	var serverTime = new Date(TimeSync.serverTime(null, 1000));
	var duration = moment.duration(end - serverTime, 'milliseconds');

	return duration.days() + ":" + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds();
});