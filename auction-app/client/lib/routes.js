/***********************
*
*	ROUTES - Iron Router
*	
*	This page contains all the routes used within the web application.
*	Routes are contolled by using Iron Router.
*
*	When a route is navigated to the Router loads the corresponding templates.
*	If no template is specified the default template is the template matching
*	the route name. e.g. Router.route('/loginPage') renders the loginPage Template.
*
*	Subscription to Publish functions are done in routes to reduce loading times 
*	throughout the application.
*
*
*************************/

// Load the homePage when the user visits the site
Router.route('/', function () {
  this.render('homePage');
});


// PUBLIC ROUTES
// These routes are available to users who are logged-in and not logged-in.
Router.route('/loginPage');
Router.route('/about');
Router.route('/homePage');

Router.route('/registerPage', {
	template: 'registerPage',
	waitOn: function() {
		return Meteor.subscribe('sectors');
	}
})


/*	PRIVATE ROUTES 
*	
*	These routes are only available to logged-in users.
*/
Router.route('/auctionBuilder', {
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});


// My Auctions
Router.route('/myAuctions', {
	template: 'myAuctions',
	data: function() {
		return auctions.find({seller: Meteor.userId()});
	},
	waitOn: function() { 
		return [
			Meteor.subscribe('userList'),
			Meteor.subscribe('sellerAuctions')
			]
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }

});

//	Watched Auctions
Router.route('/viewWatchedAuctions', {
	template: 'viewWatchedAuctions',
	data: function() {
		return auctions.find({
	    	invitedBidders: Meteor.userId()
	    })
	},
	waitOn: function() {
		return [ 
				Meteor.subscribe('userBidData'),
				//Meteor.subscribe('bidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('invitedBidderAuctions')
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

// Public Auctions
Router.route('/viewPublicAuctions', {
	template:'viewPublicAuctions',
	waitOn: function() {
		return [Meteor.subscribe('publicAuctions'),
			Meteor.subscribe('userData')];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

/*****************************************************
*	AUCTION ROUTES
*
*	These routes are for pages which display auctions and
*	their results.
******************************************************/
//	BLIND AUCTION PAGE
Router.route('/blind/:auctionIdentifier', {
	template: 'blindAuction',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({_id : mid});
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [
				//Meteor.subscribe('bidData'),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('auctionDoc', mid),
				Meteor.subscribe('auctionImageData')
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

//	BLIND AUCTION RESULTS PAGE
Router.route('/blind/results/:auctionIdentifier', {
	template: 'blindResults',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({_id : mid});		
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [ 
				//Meteor.subscribe('bidData'),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('auctionDoc', mid),
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

//	SECOND PRICE AUCTION PAGE
Router.route('/secondprice/:auctionIdentifier', {
	template: 'secondprice',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({_id : mid});
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [ 		
				//Meteor.subscribe('bidData'),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('auctionDoc', mid),
				Meteor.subscribe('auctionImageData')
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

//	SECOND PRICE AUCTION RESULTS PAGE
Router.route('/secondprice/results/:auctionIdentifier', {
	template: 'secondpriceResults',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({_id : mid});		
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [ 
				//Meteor.subscribe('bidData'),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('auctionDoc', mid),
				Meteor.subscribe('auctionImageData')
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

//	ENGLISH AUCTION PAGE
Router.route('/english/:auctionIdentifier', {
	template: 'englishAuction',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({_id : mid});
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [
				Meteor.subscribe('auctionDoc', mid),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('bidData'),
				Meteor.subscribe('userData')
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

//	ENGLISH AUCTION RESULTS PAGE
Router.route('/english/results/:auctionIdentifier', {
	template:'englishResults',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({ _id: mid });
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [
				Meteor.subscribe('bidData'),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('auctionDoc', mid)
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

//	DUTCH AUCTION PAGE
Router.route('/dutch/:auctionIdentifier', {
	template: 'dutchAuction',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({_id : mid});
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [ 
			Meteor.subscribe('userBidData'),
			//Meteor.subscribe('bidData'),
			Meteor.subscribe('userData'),
			Meteor.subscribe('auctionDoc', mid),
			];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

Router.route('/dutch/results/:auctionIdentifier', {
	template:'dutchResults',
	data: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return auctions.findOne({ _id: mid });
	},
	waitOn: function() {
		var currentAuction = this.params.auctionIdentifier;
		var mid = new Mongo.ObjectID(currentAuction);
		return [
				//Meteor.subscribe('bidData'),
				Meteor.subscribe('userBidData'),
				Meteor.subscribe('userData'),
				Meteor.subscribe('auctionDoc', mid)
				];
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

/*****************************************************
*	USER ROUTES
*
*	These routes are for user specific pages.
******************************************************/
// Friend Page
Router.route('/viewFriends', {
	template: 'viewFriends',
	waitOn: function() {
		return Meteor.subscribe('userData');
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});

// Profile Page
Router.route('/profile/:userIdentifier', {
	template: 'profilePage',
	data: function() {
		var userId = this.params.userIdentifier;
		return Meteor.users.findOne({_id : userId});
	},
	waitOn: function() {
		return [
			Meteor.subscribe('userData'),
			Meteor.subscribe('profileImages')
			]
	},
	onBeforeAction: function(){
        if(Meteor.userId()){
            this.next();
        } else {
            this.render("loginPage");
        }
    }
});