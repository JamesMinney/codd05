auctions = new Mongo.Collection('auctions', { idGeneration: 'MONGO' });
bids = new Mongo.Collection('bids');
sectors = new Mongo.Collection('sectors');

var imageStore = new FS.Store.GridFS("images", {
  maxTries: 5, // optional, default 5
  chunkSize: 1024*1024  // optional, default GridFS chunk size in bytes (can be overridden per file).
                        // Default: 2MB. Reasonable range: 512KB - 4MB
});

var createThumb = function(fileObj, readStream, writeStream) {
  // Transform the image into a 120x90 thumbnail
  gm(readStream, fileObj.name()).resize('120', '90').stream().pipe(writeStream);
};

images = new FS.Collection("images", {
  	stores: [
  		new FS.Store.GridFS("thumbs", { transformWrite: createThumb }),
  		imageStore
  	]
});

images.allow({
	download: function() {
		return true;
	},
	insert: function() {
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
})

// Profile Pics
var profilePicStore = new FS.Store.GridFS("profilePicStore", {
  maxTries: 5, // optional, default 5
  chunkSize: 1024*1024  // optional, default GridFS chunk size in bytes (can be overridden per file).
                        // Default: 2MB. Reasonable range: 512KB - 4MB
});

var createProfileThumb = function(fileObj, readStream, writeStream) {
	// Transform the image into a 150x120 thumbnail
	gm(readStream, fileObj.name()).resize('150', '120').stream().pipe(writeStream);
};

profilePics = new FS.Collection("profilePics", {
	stores: [
		new FS.Store.GridFS("profileThumbs", { transformWrite: createProfileThumb }),
		profilePicStore
	]
});

profilePics.allow({
	download: function() {
		return true;
	},
	insert: function() {
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}	
})

/*
var imageStore = new FS.Store.FileSystem("images", {
	path: "~/app-files/images", //optional, default is "/cfs/files" path within app container
	maxTries: 5 //optional, default 5
});

images = new FS.Collection("images", {
  stores: [imageStore]
});

images.allow({
	download: function() {
		return true;
	},
	insert: function() {
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}
})

// Profile Pics
var profilePicStore = new FS.Store.FileSystem("profilePicStore", {
  maxTries: 5, // optional, default 5
});

var createProfileThumb = function(fileObj, readStream, writeStream) {
	// Transform the image into a 150x120 thumbnail
	gm(readStream, fileObj.name()).resize('150', '120').stream().pipe(writeStream);
};

profilePics = new FS.Collection("profilePics", {
	stores: [
		new FS.Store.FileSystem("profileThumbs", { transformWrite: createProfileThumb }),
		profilePicStore
	]
});

profilePics.allow({
	download: function() {
		return true;
	},
	insert: function() {
		return true;
	},
	update: function(){
		return true;
	},
	remove: function(){
		return true;
	}	
})*/